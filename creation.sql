prompt "Groupe 4 - OLIVE Louis-Martial (n° 21916399)"

prompt "Création des relations :"

CREATE TABLE CLIENT (
       idClient NUMERIC(5, 0),
       nomClient VARCHAR(20),
       prenomClient VARCHAR(20),
       telephoneClient VARCHAR(15),
       Statut VARCHAR(11) DEFAULT 'NOUVEAU',
       CONSTRAINT PK_CLIENT PRIMARY KEY(idClient),
       CONSTRAINT CHECK_STATUT CHECK (Statut IN ('NOUVEAU', 'OCCASIONNEL', 'HABITUE'))
);

CREATE TABLE TABLE_ (
       numTable NUMERIC(2, 0),
       nbPlace NUMERIC(2, 0),
       CONSTRAINT PK_TABLE_ PRIMARY KEY(numTable)
);

CREATE TABLE MENU (
       idMenu NUMERIC(2, 0),
       titreMenu VARCHAR(20),
       partieJournee VARCHAR(12) DEFAULT 'MIDI_ET_SOIR',
       CONSTRAINT PK_MENU PRIMARY KEY(idMenu),
       CONSTRAINT CHECK_PARTIEJOURNEE CHECK (partieJournee IN ('MIDI', 'SOIR', 'MIDI_ET_SOIR'))
);

CREATE TABLE MET (
       idMet NUMERIC(2, 0),
       titreMet VARCHAR(20),
       typeMet VARCHAR(7) CHECK (typeMet IN ('ENTREE', 'PLAT', 'DESSERT')),
       Prix NUMERIC(4, 2),
       CONSTRAINT PK_MET PRIMARY KEY(idMet)
);

CREATE TABLE CHEF (
       idChef NUMERIC(2, 0),
       nomChef VARCHAR(20),
       prenomChef VARCHAR(20),
       telephoneChef VARCHAR(15),
       nbEtoile NUMERIC(2, 0) NOT NULL,
       Salaire NUMERIC(6, 2),
       CONSTRAINT PK_CHEF PRIMARY KEY(idChef)
);

CREATE TABLE COMPOSE (
       idMenu NUMERIC(2, 0),
       idMet NUMERIC(2, 0),
       CONSTRAINT PK_COMPOSE PRIMARY KEY(idMenu, idMet),
       CONSTRAINT FK_COMPOSE_MENU FOREIGN KEY(idMenu) REFERENCES MENU(idMenu),
       CONSTRAINT FK_COMPOSE_MET FOREIGN KEY(idMet) REFERENCES MET(idMet)
);

CREATE TABLE CUISINE (
       idChef NUMERIC(2, 0),
       idMet NUMERIC(2, 0),
       CONSTRAINT PK_CUISINE PRIMARY KEY(idChef, idMet),
       CONSTRAINT FK_CUISINE_CHEF FOREIGN KEY(idChef) REFERENCES CHEF(idChef),
       CONSTRAINT FK_CUISINE_MET FOREIGN KEY(idMet) REFERENCES MET(idMet)
);

CREATE TABLE RESERVE (
       idClient NUMERIC(5, 0),
       numTable NUMERIC(2, 0),
       idMenu NUMERIC(2, 0),
       dateReservation DATE,
       nbMemeMenu NUMERIC(2, 0) DEFAULT 1,
       CONSTRAINT PK_RESERVE PRIMARY KEY(idClient, numTable, idMenu, dateReservation),
       CONSTRAINT FK_RESERVE_CLIENT FOREIGN KEY(idClient) REFERENCES CLIENT(idClient),
       CONSTRAINT FK_RESERVE_TABLE_ FOREIGN KEY(numTable) REFERENCES TABLE_(numTable),
       CONSTRAINT FK_RESERVE_MENU FOREIGN KEY(idMenu) REFERENCES MENU(idMenu)
);

prompt "Passage des dates au format jj-mm-aaaa :"

ALTER SESSION SET NLS_DATE_FORMAT='DD-MM-YYYY HH24:MI';

prompt "Création des triggers :"

CREATE OR REPLACE TRIGGER maj_salaire
BEFORE INSERT
       ON CHEF
       FOR EACH ROW
DECLARE
       Salaire NUMBER;
BEGIN
       IF :NEW.nbEtoile = 0 THEN 
              Salaire := 1600;
       ELSIF :NEW.nbEtoile = 1 THEN
              Salaire := 1800;
       ELSIF :NEW.nbEtoile = 2 THEN
              Salaire := 2000;
       ELSE
              Salaire := 2200;
       END IF;
       :NEW.Salaire := Salaire;
END;
/

CREATE OR REPLACE TRIGGER maj_statut
BEFORE INSERT
       ON RESERVE
       FOR EACH ROW
DECLARE
       nbMenuPasse NUMBER := 0;
       nbMenuCetteCommande NUMBER := 0;
       nbMenuTotal NUMBER := 0;
       nouveauStatut VARCHAR(11);
BEGIN
       SELECT SUM(nbMemeMenu) INTO nbMenuPasse FROM RESERVE WHERE RESERVE.idClient = :NEW.idClient;
       IF nbMenuPasse IS NULL THEN
              nbMenuPasse := 0;
       END IF;
       nbMenuCetteCommande := :NEW.nbMemeMenu;
       nbMenuTotal := nbMenuCetteCommande + nbMenuPasse;
       IF nbMenuTotal >= 4 THEN
              IF nbMenuTotal >= 16 THEN
                     nouveauStatut := 'HABITUE';
              ELSE
                     nouveauStatut := 'OCCASIONNEL';
              END IF;
              UPDATE CLIENT SET Statut = nouveauStatut WHERE CLIENT.idClient = :NEW.idClient;
       END IF;
END;
/

prompt "Insertion des données :"

INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00001, 'RUBIN', 'ALBIN', '0753355162');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00002, 'ADAM', 'DUVAL', '0255271660');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00003, 'AGATHE', 'BREBION', '0697854223');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00004, 'SOPHIE', 'MOREL', '0891958907');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00005, 'CASSANDRE', 'MOREAU', '0523685769');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00006, 'HANNA', 'DUFOUR', '0345359522');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00007, 'OCTAVE', 'LEFEVRE', '0635578477');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00008, 'ALIX', 'LAMBERT', '0497685767');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00009, 'ELINA', 'HUART', '0607576361');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00010, 'ADELINE', 'GUERIN', '0811024271');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00011, 'ACHILLE', 'GAUTHIER', '0438852204');

INSERT INTO TABLE_ VALUES(01, 2);
INSERT INTO TABLE_ VALUES(02, 2);
INSERT INTO TABLE_ VALUES(03, 2);
INSERT INTO TABLE_ VALUES(04, 2);
INSERT INTO TABLE_ VALUES(05, 4);
INSERT INTO TABLE_ VALUES(06, 4);
INSERT INTO TABLE_ VALUES(07, 4);
INSERT INTO TABLE_ VALUES(08, 6);
INSERT INTO TABLE_ VALUES(09, 8);

INSERT INTO MENU(idMenu, titreMenu) VALUES(01, 'ENFANT');
INSERT INTO MENU(idMenu, titreMenu) VALUES(02, 'PETITE FIN');
INSERT INTO MENU(idMenu, titreMenu) VALUES(03, 'VEGGIE_1');
INSERT INTO MENU(idMenu, titreMenu) VALUES(04, 'VEGGIE_2');
INSERT INTO MENU(idMenu, titreMenu) VALUES(05, 'FROMAGES');
INSERT INTO MENU(idMenu, titreMenu) VALUES(06, 'TRADITION_1');
INSERT INTO MENU(idMenu, titreMenu) VALUES(07, 'TRADITION_2');
INSERT INTO MENU VALUES(08, 'SIMPLE', 'MIDI');
INSERT INTO MENU VALUES(09, 'GRANDE FIN', 'SOIR');
INSERT INTO MENU VALUES(10, 'DEGUSTATION', 'SOIR');

INSERT INTO MET VALUES(01, 'FROMAGE', 'ENTREE', 9);
INSERT INTO MET VALUES(02, 'SALADE', 'ENTREE', 7);
INSERT INTO MET VALUES(03, 'ASPERGES', 'ENTREE', 6);
INSERT INTO MET VALUES(04, 'CHAMPIGNONS', 'ENTREE', 8);
INSERT INTO MET VALUES(05, 'CHOUCROUTE', 'PLAT', 20);
INSERT INTO MET VALUES(06, 'MOULES', 'PLAT', 16);
INSERT INTO MET VALUES(07, 'RATATOUILLE', 'PLAT', 16);
INSERT INTO MET VALUES(08, 'BURGER VEGGIE', 'PLAT', 16);
INSERT INTO MET VALUES(09, 'FONDUE', 'PLAT', 18);
INSERT INTO MET VALUES(10, 'SOUFFLE', 'DESSERT', 9);
INSERT INTO MET VALUES(11, 'FLAN', 'DESSERT', 6);
INSERT INTO MET VALUES(12, 'GLACE', 'DESSERT', 6);
INSERT INTO MET VALUES(13, 'TARTE', 'DESSERT', 8);

INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(01, 'MARCEL', 'JACQUINOT', '0684747227', 0);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(02, 'JULIETTE', 'DEPARROIS', '0179533562', 0);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(03, 'THIBAULT', 'GUERIN', '0736939263', 0);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(04, 'THIMOTHE', 'MORIN', '0981559523', 1);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(05, 'VALENTINE', 'BETHULEAU', '0723805291', 1);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(06, 'EVA', 'MERCIER', '0714685837', 2);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(07, 'SARA', 'PICARD', '0695590340', 3);

INSERT INTO COMPOSE VALUES(01, 08);
INSERT INTO COMPOSE VALUES(01, 12);
INSERT INTO COMPOSE VALUES(02, 08);
INSERT INTO COMPOSE VALUES(03, 02);
INSERT INTO COMPOSE VALUES(03, 08);
INSERT INTO COMPOSE VALUES(04, 03);
INSERT INTO COMPOSE VALUES(04, 08);
INSERT INTO COMPOSE VALUES(05, 01);
INSERT INTO COMPOSE VALUES(05, 09);
INSERT INTO COMPOSE VALUES(06, 05);
INSERT INTO COMPOSE VALUES(06, 13);
INSERT INTO COMPOSE VALUES(07, 07);
INSERT INTO COMPOSE VALUES(07, 13);
INSERT INTO COMPOSE VALUES(08, 06);
INSERT INTO COMPOSE VALUES(09, 02);
INSERT INTO COMPOSE VALUES(09, 05);
INSERT INTO COMPOSE VALUES(09, 11);
INSERT INTO COMPOSE VALUES(10, 09);
INSERT INTO COMPOSE VALUES(10, 11);

INSERT INTO CUISINE VALUES(02, 01);
INSERT INTO CUISINE VALUES(02, 09);
INSERT INTO CUISINE VALUES(02, 13);
INSERT INTO CUISINE VALUES(03, 01);
INSERT INTO CUISINE VALUES(03, 03);
INSERT INTO CUISINE VALUES(03, 08);
INSERT INTO CUISINE VALUES(03, 09);
INSERT INTO CUISINE VALUES(03, 10);
INSERT INTO CUISINE VALUES(03, 11);
INSERT INTO CUISINE VALUES(03, 13);
INSERT INTO CUISINE VALUES(04, 02);
INSERT INTO CUISINE VALUES(04, 09);
INSERT INTO CUISINE VALUES(04, 11);
INSERT INTO CUISINE VALUES(04, 12);
INSERT INTO CUISINE VALUES(04, 13);
INSERT INTO CUISINE VALUES(05, 01);
INSERT INTO CUISINE VALUES(05, 02);
INSERT INTO CUISINE VALUES(05, 03);
INSERT INTO CUISINE VALUES(05, 04);
INSERT INTO CUISINE VALUES(05, 05);
INSERT INTO CUISINE VALUES(05, 06);
INSERT INTO CUISINE VALUES(05, 07);
INSERT INTO CUISINE VALUES(05, 08);
INSERT INTO CUISINE VALUES(05, 09);
INSERT INTO CUISINE VALUES(05, 10);
INSERT INTO CUISINE VALUES(05, 11);
INSERT INTO CUISINE VALUES(05, 12);
INSERT INTO CUISINE VALUES(05, 13);
INSERT INTO CUISINE VALUES(06, 04);
INSERT INTO CUISINE VALUES(06, 05);
INSERT INTO CUISINE VALUES(06, 08);
INSERT INTO CUISINE VALUES(06, 13);
INSERT INTO CUISINE VALUES(07, 01);
INSERT INTO CUISINE VALUES(07, 02);
INSERT INTO CUISINE VALUES(07, 03);
INSERT INTO CUISINE VALUES(07, 04);
INSERT INTO CUISINE VALUES(07, 05);
INSERT INTO CUISINE VALUES(07, 06);
INSERT INTO CUISINE VALUES(07, 07);
INSERT INTO CUISINE VALUES(07, 08);
INSERT INTO CUISINE VALUES(07, 09);
INSERT INTO CUISINE VALUES(07, 10);
INSERT INTO CUISINE VALUES(07, 11);
INSERT INTO CUISINE VALUES(07, 12);
INSERT INTO CUISINE VALUES(07, 13);

INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00001, 02, 06, TO_DATE('10-10-2021 21:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00001, 02, 07, TO_DATE('10-10-2021 21:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00002, 01, 04, TO_DATE('26-11-2021 13:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00002, 01, 08, TO_DATE('26-11-2021 13:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00003, 03, 01, TO_DATE('09-12-2021 12:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00004, 07, 02, TO_DATE('06-12-2021 19:30', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00004, 07, 07, TO_DATE('06-12-2021 19:30', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00004, 07, 09, TO_DATE('06-12-2021 19:30', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00004, 07, 01, TO_DATE('06-12-2021 19:30', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00005, 06, 10, TO_DATE('18-11-2021 20:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00005, 06, 07, TO_DATE('18-11-2021 20:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00005, 06, 04, TO_DATE('18-11-2021 20:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00005, 06, 02, TO_DATE('18-11-2021 20:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00005, 01, 01, TO_DATE('14-11-2021 12:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00005, 01, 08, TO_DATE('14-11-2021 12:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00009, 05, 08, TO_DATE('22-12-2021 13:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00009, 05, 04, TO_DATE('22-12-2021 13:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00009, 05, 05, TO_DATE('22-12-2021 13:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00009, 05, 07, TO_DATE('22-12-2021 13:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00010, 01, 03, TO_DATE('21-10-2021 20:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00010, 01, 04, TO_DATE('21-10-2021 20:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE VALUES(00002, 04, 02, TO_DATE('14-10-2021 20:00', 'DD-MM-YYYY HH24:MI'), 2);
INSERT INTO RESERVE VALUES(00002, 04, 04, TO_DATE('16-10-2021 21:00', 'DD-MM-YYYY HH24:MI'), 2);
INSERT INTO RESERVE VALUES(00004, 01, 07, TO_DATE('22-12-2021 20:00', 'DD-MM-YYYY HH24:MI'), 2);
INSERT INTO RESERVE VALUES(00005, 02, 06, TO_DATE('01-11-2021, 22:00', 'DD-MM-YYYY HH24:MI'), 2);
INSERT INTO RESERVE VALUES(00009, 01, 05, TO_DATE('09-11-2021, 22:00', 'DD-MM-YYYY HH24:MI'), 2);
INSERT INTO RESERVE VALUES(00010, 09, 03, TO_DATE('12-10-2021, 22:00', 'DD-MM-YYYY HH24:MI'), 8);
INSERT INTO RESERVE VALUES(00010, 08, 03, TO_DATE('01-11-2021, 22:00', 'DD-MM-YYYY HH24:MI'), 6);

prompt "1. Nom, prénom et téléphone du client qui a commandé le plus de menus. C’est une sous-requête."

SELECT nomClient, prenomClient, telephoneClient, SUM(nbMemeMenu)
FROM CLIENT JOIN RESERVE ON RESERVE.idClient = CLIENT.idClient
GROUP BY CLIENT.idClient, nomClient, prenomClient, telephoneClient
HAVING SUM(nbMemeMenu) >= ALL
	(SELECT SUM(nbMemeMenu) FROM RESERVE GROUP BY RESERVE.idClient);

prompt "2. Pour chaque client (identifié par son nom et prénom), le titre du menu le plus commandé. Un même client peut avoir commandé plusieurs menus le même nombre de fois. C’est une sous-requête corrélative."

SELECT nomClient, prenomClient, titreMenu
FROM RESERVE R_1
JOIN MENU ON MENU.idMenu = R_1.idMenu
JOIN CLIENT ON CLIENT.idClient = R_1.idClient
GROUP BY R_1.idClient, R_1.idMenu, nomClient, prenomClient, titreMenu
HAVING SUM(R_1.nbMemeMenu) >= ALL
	(SELECT SUM(R_2.nbMemeMenu) FROM RESERVE R_2  WHERE R_2.idClient = R_1.idClient GROUP BY R_2.idMenu)
ORDER BY nomClient, prenomClient;

prompt "3. Nom et prénom des chefs qui cuisinent tous les mets. C’est une division."

SELECT nomChef, prenomChef
FROM CHEF
WHERE NOT EXISTS
	(SELECT * FROM MET WHERE NOT EXISTS
		(SELECT * FROM CUISINE WHERE CUISINE.idChef = CHEF.idChef AND CUISINE.idMet = MET.idMet));

prompt "4. Pour chaque menu (identifié par son titre), le titre des mets qui le composent. Utilisation d’un GROUP BY." 

SELECT titreMenu, titreMet
FROM COMPOSE
JOIN MENU ON MENU.idMenu = COMPOSE.idMenu
JOIN MET ON MET.idMet = COMPOSE.idMet
GROUP BY MENU.idMenu, titreMenu, titreMet
ORDER BY MENU.idMenu;

prompt "5. Titre du menu qui est le plus cher à préparer. On obtient le prix d’un menu en faisant la somme du prix des mets qui le composent. C’est une sous-requête."

SELECT titreMenu, SUM(MET.Prix)
FROM MENU
JOIN COMPOSE ON COMPOSE.idMenu = MENU.idMenu
JOIN MET ON MET.idMet = COMPOSE.idMet
GROUP BY MENU.idMenu, titreMenu
HAVING SUM(MET.Prix) >= ALL
	(SELECT SUM(MET.Prix) FROM COMPOSE JOIN MET ON MET.idMet = COMPOSE.idMet GROUP BY COMPOSE.idMenu);

prompt "6. Titre des mets qui ne composent aucun menu. C’est une sous-requête."

SELECT titreMet
FROM MET
WHERE idMet NOT IN
	(SELECT idMet FROM COMPOSE);

prompt "Création de la procédure et de la fonction :"

CREATE OR REPLACE PROCEDURE augmentation_salaire_nb_plats
IS
       CURSOR mesChefs IS SELECT * FROM CHEF;
       monChef CHEF%ROWTYPE;
       ancienSalaire NUMERIC(6, 2);
       nouveauSalaire NUMERIC(6, 2);
       nbPlat NUMBER := 0;
BEGIN
       OPEN mesChefs;
       LOOP
              FETCH mesChefs INTO monChef;
              IF mesChefs%FOUND THEN
                     ancienSalaire := monChef.Salaire;
                     SELECT COUNT(*) INTO nbPlat FROM CUISINE WHERE CUISINE.idChef = monChef.idChef;
                     nouveauSalaire := ancienSalaire + nbPlat * ancienSalaire / 100;
                     UPDATE CHEF SET CHEF.Salaire = nouveauSalaire WHERE CHEF.idChef = monChef.idChef;
              END IF;
              EXIT WHEN mesChefs%NOTFOUND;
       END LOOP;
       CLOSE mesChefs;
END;
/

CREATE OR REPLACE FUNCTION nombre_plat_cuisine (identifiantChef IN NUMBER)
RETURN NUMBER IS
       nbPlat NUMBER := -1;
       idChefTmp NUMBER;
BEGIN
       SELECT idChef INTO idChefTmp FROM CHEF WHERE idChef = identifiantChef;
       SELECT COUNT(*) INTO nbPlat FROM CUISINE WHERE CUISINE.idChef = identifiantChef;
       RETURN (nbPlat);
EXCEPTION
       WHEN NO_DATA_FOUND THEN RETURN nbPlat;
END;
/
