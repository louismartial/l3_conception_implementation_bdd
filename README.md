# Système d'information et base de données – Projet

Le projet consiste à concevoir et implémenter une base de données sur un thème choisi. Le thème de la base de données sera validé par l’enseignant.

## Sujet

Vous devez concevoir et implémenter une base de données. Pour cela, vous concevrez le modèle conceptuel E/A, puis le modèle logique relationnel et en enfin vous implémenterez le modèle physique. Le modèle logique relationnel de votre base de données devra contenir **au minimum 8 relations**, le modèle physique contiendra donc **au minimum 8 tables**. Vous devrez également peupler votre base de données, chaque table devra contenir **un minimum d’une dizaine de lignes** et permettre de tester quelques requêtes.

Vous devrez écrire et exécuter **au moins 5 requêtes** dont :

* Une requête utilisant un *group by* ;
* Une requête utilisant une division ;
* Une requête contenant une/des sous requête(s) ;
* Une requête contenant une/des sous requête(s) corrélative(s).

Vous devrez également créer **au moins 1 procédure, 1 fonction et 2 triggers**.

L’implémentation de votre base de données pourra se faire sous ORACLE, MYSQL ou POSTGRESQL.

## Rendu

### Un rapport d'une dizaine de pages maximum contenant :

* Première page : titre du rapport, numéro(s) et nom(s) du/des étudiant(s) ;
* La description du thème et sujet choisi expliquant le besoin de concevoir cette base de données ;
* Le dictionnaire des données = tableau des données/attributs indiquant pour chacun le nom/code, la description, le type de données, la longueur, la nature (obligatoire /facultatif / unique), la règle de calcul ;
* Le schéma entité-association avec les explications du schéma ;
* Le schéma relationnel associé ;
* Le schéma physique associé (les tables, attributs) ;
* Les requêtes accompagnées d’une explication ;
* L’explication des procédures, fonctions et triggers associés ;
* Les tests effectués;
* Dernière page.

### Un fichier *creation*

Contient les ordres de création des tables, des triggers, des procédures/fonctions, des insertions et les requêtes. Ce fichier contient en entête le/les auteur(s).

### Un fichier *test*

Contient des requêtes de test des triggers, des procédures et des fonctions.

### Un fichier *suppression*

Contient uniquement les ordres de suppression de la base de données.

## Évaluation

La note tiendra compte de la présentation du rapport et du code développé (éviter les procédures, fonctions et triggers trop simples).

Outre l’analyse du code sur papier, l’évaluation se fera en important et exécutant les fichiers sql que vous aurez rendu : *creation.sql*, *test.sql*, *suppression.sql*.

Il est donc fortement recommandé de tester que tout fonctionne sur vos machines avant de déposer les fichiers.