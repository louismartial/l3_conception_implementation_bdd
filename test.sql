prompt "Groupe 4 - OLIVE Louis-Martial (n° 21916399)"

prompt "1. Test du trigger maj_salaire. On insère des chefs sans préciser leur salaire. Mais les salaires sont bien présents lors du SELECT."

INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(09, 'JULIEN', 'DECHANEL', '0716283381', 0);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(10, 'AUBIN', 'GAUTIER', '06665351882', 2);
INSERT INTO CHEF(idChef, prenomChef, nomChef, telephoneChef, nbEtoile) VALUES(11, 'ANASTASIE', 'GENET', '0688993881', 4);

SELECT nomChef, nbEtoile, Salaire FROM CHEF WHERE idChef IN (09, 10, 11);

prompt "2. Test du trigger maj_statut. On insère trois nouveaux clients dont le Statut est ‘NOUVEAU’ par défaut. Puis on insère pour le client 13 une seule réservation. Son Statut reste donc ‘NOUVEAU’. On insère pour le client 14 une réservation de quatre menus. Son Statut passe donc à ‘OCCASIONNEL’. On insère pour le client 15 deux réservations de huit menus chacune. Son Statut passe donc à ‘HABITUE’."

INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00013, 'NORA', 'RAE', '0677778364');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00014, 'CHRISTELLE', 'MAROT', '0438852204');
INSERT INTO CLIENT(idClient, prenomClient, nomClient, telephoneClient) VALUES(00015, 'PIERRE', 'MICHEL', '0463335187');

prompt "AVANT :"

SELECT idClient, nomClient, Statut FROM CLIENT WHERE idClient IN (13, 14, 15);

INSERT INTO RESERVE(idClient, numTable, idMenu, dateReservation) VALUES(00013, 01, 01, TO_DATE('13-12-2021 20:00', 'DD-MM-YYYY HH24:MI'));
INSERT INTO RESERVE VALUES(00014, 05, 01, TO_DATE('12-12-2021 23:00', 'DD-MM-YYYY HH24:MI'), 5);
INSERT INTO RESERVE VALUES(00015, 09, 01, TO_DATE('13-12-2021 23:00', 'DD-MM-YYYY HH24:MI'), 8);
INSERT INTO RESERVE VALUES(00015, 09, 01, TO_DATE('14-12-2021 23:00', 'DD-MM-YYYY HH24:MI'), 8);

prompt "APRES :"

SELECT idClient, nomClient, Statut FROM CLIENT WHERE idClient IN (13, 14, 15);

prompt "3. Avant l’exécution de la procédure, les salaires sont ronds. Après l’exécution, les salaires ont été augmenté, à l’exception du salaire des chefs ‘DECHANEL’, ‘JACQUINOT’, ‘GAUTIER’ et ‘GRENET’ qui ne cuisinent aucun plat. Le salaire de ‘PICARD’, qui cuisine les 13 plats, augmente de 13 %, et passe de 2200 à 2486."

prompt "AVANT :"

SELECT nomChef, Salaire FROM CHEF;

EXECUTE augmentation_salaire_nb_plats

prompt "APRES :"

SELECT nomChef, Salaire FROM CHEF;

prompt "4. Test de la fonction nombre_plat_cuisine. Le chef d’identifiant 01 ne cuisine aucun met. Le chef d’identifiant 02 cuisine 3 mets. Le chef d’identifiant 08 n’existe pas."

prompt "Nombre de mets que cuisine le chef 01 :"

SELECT nombre_plat_cuisine(01) FROM DUAL;

prompt "Nombre de mets que cuisine le chef 02 :"

SELECT nombre_plat_cuisine(02) FROM DUAL;

prompt "Nombre de mets que cuisine le chef 08 :"

SELECT nombre_plat_cuisine(08) FROM DUAL;
